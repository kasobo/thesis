# -*- coding: utf-8 -*-
"""
Created on Thu Apr  6 10:33:55 2017

@author: TENG
"""

import os
import numpy as np
import tensorflow as tf
import bbb
from keras.datasets import imdb
from keras.preprocessing import sequence


def main(argv=None):

    # load the dataset but only keep the top n words, zero the rest
    top_words = 5000
    (X_train, Y_train), (X_test, Y_test) = imdb.load_data(num_words=top_words)
    # truncate and pad input sequences
    max_review_length = 200
    X_train = sequence.pad_sequences(X_train, maxlen=max_review_length)
    conf = bbb.Config()
    conf.layer_nodes = [200, 100]
    conf.batch_size = 200
    conf.example_size = len(X_train)
    conf.sample_times = 3
    conf.multi_mu = [0.0, 0.0]
    conf.multi_sigma = [np.exp(-1.0, dtype=np.float32), np.exp(-6.0, dtype=np.float32)]
    conf.multi_ratio = [0.25, 0.75]
    conf.learning_rate_base = 0.001
    conf.learning_rate_decay = 0.99
    conf.optimizer_api = "AdamOptimizer"

    data = tf.placeholder(tf.float32, shape= [None, max_review_length])
    target = tf.placeholder(tf.float32, shape = [None, 1])
    nn = bbb.BBB(data, target, conf, is_training=True)

    model_name = "model"
    model_save_path = "./model"
    if not os.path.isdir(model_save_path):
        os.makedirs(model_save_path)

    def next_batch(num, data, labels):
        '''
        Return a total of `num` random samples and labels.
        '''
        idx = np.arange(0 , len(data))
        np.random.shuffle(idx)
        idx = idx[:num]
        x = data[idx]
        y = labels[idx]
        y = np.asarray(y.reshape(len(y), 1))
        return x, y

    saver = tf.train.Saver()
    with tf.Session() as sess:
        tf.global_variables_initializer().run()


        for i in range(100000):
            x, y = next_batch(conf.batch_size, X_train, Y_train)
            _, loss_value, step, lr = sess.run(
                [nn.optimize, nn.loss, nn.global_step, nn.learning_rate],
                feed_dict={nn.data:x, nn.target:y})

            if i % 1000 == 0:
                print("After %d training step(s), lr(%.6f), loss on training batch is %g." % (step, lr, loss_value))
                saver.save(sess, os.path.join(model_save_path, model_name), global_step=nn.global_step)

if __name__ == "__main__":
    tf.app.run()
