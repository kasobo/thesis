import time
import tensorflow as tf
from keras.datasets import imdb
from keras.preprocessing import sequence
import numpy as np
import bbb
import random

def main(argv=None):

    top_words = 5000
    test_split = 0.25
    (X_train, Y_train), (X_test, Y_test) = imdb.load_data(num_words=top_words)
    # truncate and pad input sequences
    X_test = X_test[:5000]
    Y_test = Y_test[:5000]
    max_review_length = 200
    X_test = sequence.pad_sequences(X_test, maxlen=max_review_length)

    conf = bbb.Config()
    conf.layer_nodes = [200, 100]
    conf.example_size = 5000
    conf.target_onehot = True
    # conf.optimizer_api = "GradientDescentOptimizer"
    conf.optimizer_api = "AdamOptimizer"

    data = tf.placeholder(tf.float32, [None, max_review_length])
    target = tf.placeholder(tf.int64, [None])
    validate_feed = {
        data: X_test,
        target: Y_test}

    nn = bbb.BBB(data, target, conf, is_training=False)

    model_save_path = "./model"

    saver = tf.train.Saver()
    while True:
        with tf.Session() as sess:
            ckpt = tf.train.get_checkpoint_state(model_save_path)
            if ckpt and ckpt.model_checkpoint_path:
                saver.restore(sess, ckpt.model_checkpoint_path)
                global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]
                accuracy_score = sess.run(nn.accuracy, feed_dict=validate_feed)
                print("After %s training step(s), validation accuracy = %g" % (global_step, accuracy_score))
            else:
                print('No checkpoint file found')
                return

        time.sleep(10)


if __name__ == "__main__":
    tf.app.run()
